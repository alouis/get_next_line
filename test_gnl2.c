#include "get_next_line.h"

int	main()
{
	int fd;
	char *buff = NULL;
	int ret;

	ret = 1;
	fd = open("./automatic_test", O_RDONLY);	
	while (ret == 1)
	{
		ret = get_next_line(fd, &buff);
		printf("[%d] : %s\n", ret, buff);
		free(buff);
		buff = NULL;
	}
	return (1);
}
