#include "get_next_line.h"

/*static void		ft_putstr(int fd, char *str)
{
		write(fd, &str, ft_strlen(str));
}*/

void	ft_putstr(int fd, char *s)
{
	int i;

	i = 0;
	if (!s || !fd)
		return ((void)NULL);
	while (s[i])
	{
		write(fd, &s[i], 1);
		i++;
	}
}


static void	ft_putchar(int fd, char c)
{
	write(fd, &c, 1);
}

int main(int argc, char *argv[])
{
	int fd;
	int i;
	char *line = NULL;
	int result;
	
	i = 0;
	result = 1;
	if (argc ==  1)
	{
		fd = open("automatic_test", O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
		if (fd == -1)
		{
			write(1, "-1, open() failed", 17);
			return (0);
		}
		while (i < 2)
		{
			ft_putstr(fd, "so far, everything's alright !");
			ft_putchar(fd, '\n');
			ft_putchar(fd, '\n');
			i++;
		}
		ft_putstr(fd, "last line");
	}

	if (argc == 2)
	{
		fd = open(argv[1], O_RDONLY);
		if (fd == -1)
		{
			write(1, "-1, open() failed", 17);
			return (0);
		}
	}
	while (result == 1) 
	{
		result = get_next_line(fd, &line);
		printf("[%d] : %s\n", result, line);
		free(line);
		line = NULL;
	}
	return (1);
}

/*int	main()
{
	int fd;
	char *buff = NULL;
	int ret;

	ret = 1;
	fd = open("./script", O_RDONLY);
	while (ret >= 0)
	{
		ret = get_next_line(fd, &buff);
		printf("[%d] : %s\n", ret, buff);
		free(buff);
		buff = NULL;
	}
	return (1);
}*/
