/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/20 12:50:36 by alouis            #+#    #+#             */
/*   Updated: 2019/11/28 13:45:41 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

# ifndef BUFFER_SIZE
#  define BUFFER_SIZE 1
# endif

# include <unistd.h>
# include <stdio.h>
# include <stdlib.h>
# include <sys/stat.h>
# include <sys/types.h>
# include <fcntl.h>

int			get_next_line(int fd, char **line);
char		*free_str(char *str);

size_t		ft_strlen(char *s);
char		*ft_strjoin(char *s1, char *s2);
char		*ft_substr(char *s);
char		*ft_strdup(char *dst);
char		*ft_calloc(size_t count, size_t size);

#endif
